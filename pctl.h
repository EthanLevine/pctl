/******************************************************************************
 * FEATURE MACROS                                                             *
 ******************************************************************************/

/*
 * This macro should expand to non-zero if the complex data type (as defined by
 * C99) is supported.  Note: Some compilers had non-standard notions of complex
 * data types with different syntax; these non-standard forms are not supported.
 */
#ifndef PCTL_USE_COMPLEX
# if __STDC_VERSION__ >= 201112L
#  ifdef __STDC_NO_COMPLEX__
#   define PCTL_USE_COMPLEX 0
#  else
#   define PCTL_USE_COMPLEX 1
#  endif
# elif __STDC_VERSION__ >= 199901L
#  if __STDC_HOSTED__
#   define PCTL_USE_COMPLEX 1
#  else
#   define PCTL_USE_COMPLEX 0
#  endif
# else
#  define PCTL_USE_COMPLEX 0
# endif
#endif

/*
 * This macro should expand to non-zero if the "long long" data type is
 * supported.
 */
#ifndef PCTL_USE_LONGLONG
# if __STDC_VERSION__ >= 199901L
#  define PCTL_USE_LONGLONG 1
# else
#  include <limits.h>
#  ifdef LLONG_MAX
#   define PCTL_USE_LONGLONG 1
#  else
#   define PCTL_USE_LONGLONG 0
#  endif
# endif
#endif

/*
 * This macro should expand to non-zero if a generic assert_equals() should be
 * generated
 */
#ifndef PCTL_USE_GENERIC
# if __STDC_VERSION__ >= 201112L
#  define PCTL_USE_GENERIC 1
# else
#  define PCTL_USE_GENERIC 0
# endif
#endif

/*
 * This macro should expand to the current function, or "" if it is not
 * available.
 */
#ifndef PCTL_FUNCTION_MACRO
# if __STDC_VERSION__ >= 199901L
#  define PCTL_FUNCTION_MACRO __func__
# elif __GNUC__ >= 2
#  define PCTL_FUNCTION_MACRO __FUNCTION__
# else
#  define PCTL_FUNCTION_MACRO 0
#endif

/*
 * This macro should expand to the name of the function used to print messages.
 * Typically, this macro is defined to be "printf", but you can provide any
 * implementation you'd like.  The prototype of the function will be declared as
 *     int PCTL_PRINTF(const char *format, ...);
 */
#ifndef PCTL_PRINTF
# include <stdio.h>
# define PCTL_PRINTF printf
#endif

/*
 * This macro should expand to 0 if the 'char' type is unsigned, and 1 if it is
 * signed.  The <limits.h> header must be included if this macro is not set by
 * the user.
 */
#ifndef PCTL_IS_CHAR_SIGNED
# include <limits.h>
# if CHAR_MIN < 0
#  define PCTL_IS_CHAR_SIGNED 1
# else
#  define PCTL_IS_CHAR_SIGNED 0
# endif
#endif

/*
 * This macro should expand to '1' if all char types should be printed as ASCII
 * characters, and '0' if all char types should be printed as their numeric
 * values.
 */
#ifndef PCTL_CHAR_FORMAT
# define PCTL_CHAR_FORMAT 0
#endif

/*
 * This macro should expand to a _string_ containing the format specifier for
 * printing floating-point numbers.  For example, "f", "e", "E", "g", or "G",
 * according to the C standard, although your implementation of PCTL_PRINTF may
 * support other specifiers as well.
 */
#ifndef PCTL_FLOATING_POINT_FORMAT
# define PCTL_FLOATING_POINT_FORMAT "G"
#endif

/*
 * This macro should expand to the precision of printed floating-point values.
 */
#ifndef PCTL_FLOATING_POINT_PRECISION
# define PCTL_FLOATING_POINT_PRECISION 8
#endif

/*
 * This macro should expand to 0 if the filename should be included on the line
 * with the suite, test, and function names, and 1 if the filename should be
 * on its own line.
 */
#ifndef PCTL_FILE_ON_OWN_LINE
# define PCTL_FILE_ON_OWN_LINE 0
#endif


/******************************************************************************
 * UTILITY MACROS                                                             *
 ******************************************************************************/

#define _PCTL_STRINGIFY_(x) #x
#define _PCTL_STRINGIFY(x) _PCTL_STRINGIFY_(x)


/******************************************************************************
 * ASSERTION MACROS                                                           *
 ******************************************************************************/

/*
 * Always fail
 */
#define assert_fail(message) \
	_pctl_assert_fail(message, __FILE__, PCTL_FUNCTION_MACRO, __LINE__)

void _pctl_assert_fail(const char *message, const char *file,
		const char *function, unsigned int line);

/*
 * Boolean (really 'int') assertions
 */
#define assert_true(test) \
	_pctl_assert_true(test, _PCTL_STRINGIFY(test), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_false(test) \
	_pctl_assert_false(test, _PCTL_STRINGIFY(test), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)

void _pctl_assert_true(int test, const char *src, const char *file,
		const char *function, unsigned int line);
void _pctl_assert_false(int test, const char *src, const char *file,
		const char *function, unsigned int line);

/*
 * Numeric equality assertions
 */
#define _pctl_declare_num_assert_eq(type, type_name) \
	void _pctl_assert_##type_name##_equal(type actual, type expected, \
		const char *src, const char *file, const char *function, \
		unsigned int line);

#define assert_char_equal(actual, expected) \
	_pctl_assert_char_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_schar_equal(actual, expected) \
	_pctl_assert_schar_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_uchar_equal(actual, expected) \
	_pctl_assert_uchar_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_short_equal(actual, expected) \
	_pctl_assert_short_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_ushort_equal(actual, expected) \
	_pctl_assert_ushort_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_int_equal(actual, expected) \
	_pctl_assert_int_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_uint_equal(actual, expected) \
	_pctl_assert_uint_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_long_equal(actual, expected) \
	_pctl_assert_long_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_ulong_equal(actual, expected) \
	_pctl_assert_ulong_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_float_equal(actual, expected) \
	_pctl_assert_float_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_double_equal(actual, expected) \
	_pctl_assert_double_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_ldouble_equal(actual, expected) \
	_pctl_assert_ldouble_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)

_pctl_declare_num_assert_eq(char, char)
_pctl_declare_num_assert_eq(signed char, schar)
_pctl_declare_num_assert_eq(unsigned char, uchar)
_pctl_declare_num_assert_eq(short, short)
_pctl_declare_num_assert_eq(unsigned short, ushort)
_pctl_declare_num_assert_eq(int, int)
_pctl_declare_num_assert_eq(unsigned int, uint)
_pctl_declare_num_assert_eq(long, long)
_pctl_declare_num_assert_eq(unsigned long, ulong)
_pctl_declare_num_assert_eq(float, float)
_pctl_declare_num_assert_eq(double, double)
_pctl_declare_num_assert_eq(long double, ldouble)

/*
 * Complex number support (optional)
 */
#if PCTL_USE_COMPLEX
#define assert_floatc_equal(actual, expected) \
	_pctl_assert_floatc_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_doublec_equal(actual, expected) \
	_pctl_assert_doublec_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_ldoublec_equal(actual, expected) \
	_pctl_assert_ldoublec_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)

_pctl_declare_num_assert_eq(float _Complex, floatc)
_pctl_declare_num_assert_eq(double _Complex, doublec)
_pctl_declare_num_assert_eq(long double _Complex, ldoublec)
#endif
#endif

/*
 * Long long support (optional)
 */
#if PCTL_USE_LONGLONG
#define assert_llong_equal(actual, expected) \
	_pctl_assert_llong_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_ullong_equal(actual, expected) \
	_pctl_assert_ullong_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)

_pctl_declare_num_assert_eq(long long, llong)
_pctl_declare_num_assert_eq(unsigned long long, ullong)
#endif

/*
 * Generic equality
 */
#if PCTL_USE_GENERIC
#define assert_equal_common1_(actual, expected) _Generic((actual), \
    char: assert_char_equal(actual,expected), \
    signed char: assert_schar_equal(actual,expected), \
    unsigned char: assert_uchar_equal(actual,expected), \
    short: assert_short_equal(actual,expected), \
    unsigned short: assert_ushort_equal(actual,expected), \
    int: assert_int_equal(actual,expected), \
    unsigned int: assert_uint_equal(actual,expected), \
    long: assert_long_equal(actual,expected), \
    unsigned long: assert_ulong_equal(actual,expected), \
    float: assert_float_equal(actual,expected), \
    double: assert_double_equal(actual,expected), \
    long double: assert_ldouble_equal(actual,expected))
# if PCTL_USE_COMPLEX
#  define assert_equal_common2_(actual, expected) _Generic((actual), \
    float _Complex: assert_floatc_equal(actual,expected), \
    double _Complex: assert_doublec_equal(actual,expected), \
    long double _Complex: assert_ldoublec_equal(actual,expected), \
    default: assert_equal_common1_(actual,expected))
# else
#  define assert_equal_common2_ assert_equal_common1_
# endif
# if PCTL_USE_LONGLONG
#  define assert_equal_common3_(actual, expected) _Generic((actual), \
    long long: assert_llong_equal(actual,expected), \
    unsigned long long: assert_ullong_equal(actual,expected), \
    default: assert_equal_common2_(actual,expected))
# else
#  define assert_equal_common3_ assert_equal_common2_
# endif
#define assert_equal assert_equal_common3_
#endif

/*
 * Bitmask assertions
 */
#define assert_bitmask_set(bitfield, bitmask) \
	_pctl_assert_bitmask_set((int)(bitfield & bitmask), _PCTL_STRINGIFY(bitfield), \
		_PCTL_STRINGIFY(bitmask), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_bitmask_unset(bitfield, bitmask) \
	_pctl_assert_bitmask_unset((int)(~bitfield & bitmask), _PCTL_STRINGIFY(bitfield), \
		_PCTL_STRINGIFY(bitmask), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)

void _pctl_assert_bitmask_set(int result, const char *bf_src, const char *bm_src,
		const char *file, const char *function, unsigned int line);
void _pctl_assert_bitmask_unset(int result, const char *bf_src, const char *bm_src,
		const char *file, const char *function, unsigned int line);

/*
 * Raw memory assertions
 */
#define assert_mem_equal(actual, expected, length) \
	_pctl_assert_mem_equal(actual, expected, length, _PCTL_STRINGIFY(actual), \
		_PCTL_STRINGIFY(expected), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)

void _pctl_assert_mem_equal(const void *actual, const void *expected,
		int length, const char *a_src, const char *e_src,
		const char *file, const char *function, unsigned int line);

/*
 * String assertions
 */
#define assert_string_equal(actual, expected) \
	_pctl_assert_string_equal(actual, expected, _PCTL_STRINGIFY(actual), \
		__FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_string_contains(haystack, needle) \
	_pctl_assert_string_contains(haystack, needle, _PCTL_STRINGIFY(haystack), \
		_PCTL_STRINGIFY(needle), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_string_doesnt_contain(haystack, needle) \
	_pctl_assert_string_doesnt_contain(haystack, needle, _PCTL_STRINGIFY(haystack), \
		_PCTL_STRINGIFY(needle), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_string_starts_with(whole, start) \
	_pctl_assert_string_starts_with(whole, start, _PCTL_STRINGIFY(whole), \
		_PCTL_STRINGIFY(start), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)
#define assert_string_ends_width(whole, end) \
	_pctl_assert_string_ends_with(whole, end, _PCTL_STRINGIFY(whole), \
		_PCTL_STRINGIFY(end), __FILE__, PCTL_FUNCTION_MACRO, __LINE__)

void _pctl_assert_string_equal(const char *actual, const char *expected,
		const char *src, const char *file, const char *function,
		unsigned int line);
void _pctl_assert_string_contains(const char *haystack, const char *needle,
		const char *h_src, const char *n_src, const char *file,
		const char *function, unsigned int line);
void _pctl_assert_string_doesnt_contain(const char *haystack, const char *needle,
		const char *h_src, const char *n_src, const char *file,
		const char *function, unsigned int line);
void _pctl_assert_string_starts_with(const char *whole, const char *start,
		const char *w_src, const char *s_src, const char *file,
		const char *function, unsigned int line);
void _pctl_assert_string_ends_width(const char *whole, const char *end,
		const char *w_src, const char *e_src, const char *file,
		const char *function, unsigned int line);

int pctl_suites_passed(void);
int pctl_suites_failed(void);

int pctl_tests_passed(void);
int pctl_tests_failed(void);

int pctl_asserts_passed(void);
int pctl_asserts_failed(void);


typedef void (*pctl_suite_fn)(void);
typedef void (*pctl_test_fn)(void);
typedef void (*pctl_setup_fn)(void);
typedef void (*pctl_teardown_fn)(void);

void pctl_run_suite(pctl_suite_fn suite, const char *name);
void pctl_set_setup(pctl_setup_fn setup);
void pctl_set_teardown(pctl_teardown_fn teardown);
void pctl_run_test(pctl_test_fn test, const char *name);

void pctl_print_summary(void);
