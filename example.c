#include <stdbool.h>
#include <stdlib.h>

#include "pctl.h"

/*
 * This file includes a couple basic functions along with some tests for them.
 *
 * Obviously nobody of sane mind would actually re-implement strlen() and
 * strstr(), especially seeing as the real strstr() returns the substring.
 *
 * Build this file with a command like this:
 *    gcc pctl.c example.c -o example
 */

static size_t my_strlen(const char *str)
{
    size_t len = 0;

    if (str == NULL)
        return 0;

    for (; *str != 0; str++)
        len++;

    return len;
}        

/* note: prior to C99 you would probably use a homegrown bool type here */
static bool my_strstr(const char *haystack, const char *needle)
{
    if (haystack == NULL)
        return false;
    if (needle == NULL)
        return false;

    size_t needle_len = my_strlen(needle);

    /* note: without this, my_strstr("", "") returns false */
    if (needle_len == 0)
        return true;

    while (*haystack != 0)
    {
        size_t i = 0;

        /* try to find 'needle' starting at 'haystack' */
        for (i = 0; i < needle_len; i++)
        {
            if (haystack[i] != needle[i])
                break;
        }

        /* if 'i' did reached 'needle_len', then we found a match here */
        if (i == needle_len)
            return true;

        /* otherwise, keep looking! */
        haystack++;
    }

    /* we've exhausted the haystack, we couldn't find the needle */
    return false;
}

/*
 * TEST FUNCTIONS
 */
static void test_strlen(void)
{
    /* note: technically we should use size_t, but it should never be larger
     *       than unsigned long */
    assert_ulong_equal(my_strlen(NULL), 0);
    assert_ulong_equal(my_strlen(""), 0);
    assert_ulong_equal(my_strlen("abc"), 3);
    assert_ulong_equal(my_strlen("weird\0case"), 5); /* string terminates at NUL */
}

static void test_strstr_basic(void)
{
    assert_true(my_strstr("hello world", "hello"));
    assert_true(my_strstr("hello world", "world"));
    assert_true(my_strstr("hello world", "llo w"));
}

static void test_strstr_null(void)
{
    assert_false(my_strstr(NULL, NULL));
    assert_false(my_strstr(NULL, "hello"));
    assert_false(my_strstr("hello", NULL));
}

static void test_strstr_empty(void)
{
    assert_true(my_strstr("hello", ""));
    assert_true(my_strstr("", ""));
    assert_false(my_strstr("", "hello"));
}

static void test_strstr_edges(void)
{
    assert_true(my_strstr("hello world", "hello world"));
    assert_false(my_strstr("hello world", "rld__"));
    assert_true(my_strstr("hellollo world", "llo w"));
}

static void test_strstr_alias(void)
{
    /* note: the compiler will probably coalesce equivalent strings in the
     *       same TU, but this is here just in case */
    const char *test_str = "alias test";
    assert_true(my_strstr(test_str, test_str));
}

/*
 * SUITE FUNCITONS
 */
static void suite_strlen(void)
{
    pctl_run_test(test_strlen, "strlen test");
}

static void suite_strstr(void)
{
    pctl_run_test(test_strstr_basic, "strstr basic");
    pctl_run_test(test_strstr_null, "strstr null");
    pctl_run_test(test_strstr_empty, "strstr empty");
    pctl_run_test(test_strstr_edges, "strstr edge cases");
    pctl_run_test(test_strstr_alias, "strstr aliasing");
}

int main(int argc, char **argv)
{
    pctl_run_suite(suite_strlen, "strlen");
    pctl_run_suite(suite_strstr, "strstr");

    pctl_print_summary();

    return pctl_asserts_failed();
}

