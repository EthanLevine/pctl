#include "pctl.h"

#ifndef NULL
# define NULL			0
#endif
#define TRUE			1
#define FALSE			0

/******************************************************************************
 * Structure definitions                                                      *
 ******************************************************************************/
struct pctl_suite {
	pctl_suite_fn fn;
	const char *name;
};

struct pctl_test {
	pctl_test_fn fn;
	const char *name;
};


/******************************************************************************
 * Static globals                                                             *
 ******************************************************************************/
static struct pctl_suite executing_suite = {0};
static struct pctl_test executing_test = {0};
static pctl_setup_fn executing_suite_setup = NULL;
static pctl_teardown_fn executing_suite_teardown = NULL;
static int executing_suite_failed_asserts = 0;
static int executing_test_failed_asserts = 0;

static int suites_passed = 0;
static int suites_failed = 0;
static int tests_passed = 0;
static int tests_failed = 0;
static int asserts_passed = 0;
static int asserts_failed = 0;


/******************************************************************************
 * Helper function declarations                                               *
 ******************************************************************************/
static int pctl_str_eq(const char *str_a, const char *str_b);
static int pctl_str_starts(const char *whole, const char *start);
static int pctl_str_ends(const char *whole, const char *end);
static int pctl_str_contains(const char *haystack, const char *needle);

/*
 * Declare configurable support functions.
 */
int PCTL_PRINTF(const char *format, ...);


/******************************************************************************
 * Helper function definitions                                                *
 ******************************************************************************/
static int pctl_str_eq(const char *str_a, const char *str_b)
{
	while (*str_a != '\0' && *str_b != '\0') {
		if (*str_a != *str_b)
			return FALSE;
		str_a++;
		str_b++;
	}
	if (*str_a == *str_b)
		return TRUE;
	else
		return FALSE;
}

static int pctl_str_starts(const char *whole, const char *start)
{
	while (*whole != '\0' && *start != '\0') {
		if (*whole != *start)
			return FALSE;
		whole++;
		start++;
	}
	if (*start == '\0')
		return TRUE;
	else
		return FALSE;
}

static int pctl_str_ends(const char *whole_head, const char *end_head)
{
	const char *whole;
	const char *end;
	for (whole = whole_head; *whole != '\0'; whole++);
	for (end = end_head; *end != '\0'; end++);
	while (whole_head != whole && end_head != end) {
		if (*whole != *end)
			return FALSE;
		whole--;
		end--;
	}
	if (end == end_head)
		return TRUE;
	else
		return FALSE;
}

static int pctl_str_contains(const char *haystack, const char *needle)
{
	while (*haystack != '\0') {
		if (pctl_str_starts(haystack, needle))
			return TRUE;
		haystack++;
	}
	return FALSE;
}

/* Prints:

suite {suite}, test {test}, in {function},
at {/path/to/file}:{line}
.... (reason)

*/
static void register_assert_fail(const char *file, const char *function,
		unsigned int line)
{
	executing_suite_failed_asserts++;
	executing_test_failed_asserts++;
	asserts_failed++;

	if (executing_suite.fn != NULL) {
		if (executing_suite.name == NULL)
			PCTL_PRINTF("anonymous suite, ");
		else
			PCTL_PRINTF("suite %s, ", executing_suite.name);
	}

	if (executing_test.name == NULL)
		PCTL_PRINTF("anonymous test, ");
	else
		PCTL_PRINTF("test %s, ", executing_test.name);

	if (function != NULL) {
		PCTL_PRINTF("in %s", function);
#if PCTL_FILE_ON_OWN_LINE
		PCTL_PRINTF("\n");
#else
		PCTL_PRINTF(", ");
#endif
	}
	PCTL_PRINTF("at %s:%d\n", file, line);
}

static void register_assert_pass()
{
	asserts_passed++;
}

/******************************************************************************
 * Assertion implementations                                                  *
 ******************************************************************************/

/*
 * Always fail
 */ 
void _pctl_assert_fail(const char *message, const char *file,
		const char *function, unsigned int line)
{
	register_assert_fail(file, function, line);
	PCTL_PRINTF("Failure asserted: %s\n\n", message);
}

/*
 * Boolean (really 'int') assertions
 */
void _pctl_assert_true(int test, const char *src, const char *file,
		const char *function, unsigned int line)
{
	if (test) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected true, but given false in:\n    %s\n\n",
				src);
	}
}

void _pctl_assert_false(int test, const char *src, const char *file,
		const char *function, unsigned int line)
{
	if (!test) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected false, but given true in:\n    %s\n\n",
				src);
	}
}

/*
 * Numeric equality assertions
 */

/* Used to define basic numerical equality assertions */
#define _pctl_define_num_assert_eq(type, type_name, format_spec) \
	void _pctl_assert_##type_name##_equal(type actual, type expected, \
		const char *src, const char *file, const char *function, \
		unsigned int line) \
	{ \
		if (actual == expected) { \
			register_assert_pass(); \
		} else { \
			register_assert_fail(file, function, line); \
			PCTL_PRINTF("Expected " format_spec ", but given " \
					format_spec " in\n    %s\n\n", \
					expected, actual, src); \
		} \
	}

/* Used to define floating-point equality assertions (requires precision) */
#define _pctl_define_float_assert_eq(type, type_name, format_char, precision) \
	_pctl_define_num_assert_eq(type, type_name, "%" _PCTL_STRINGIFY(precision) format_char)

/* Integer equality assertions */
#if PCTL_CHAR_FORMAT == 0
_pctl_define_num_assert_eq(signed char, schar, "%i")
_pctl_define_num_assert_eq(unsigned char, uchar, "%u")
# if PCTL_IS_CHAR_SIGNED
_pctl_define_num_assert_eq(char, char, "%i")
# else
_pctl_define_num_assert_eq(char, char, "%u")
# endif
#else
_pctl_define_num_assert_eq(char, char, "%c")
_pctl_define_num_assert_eq(signed char, schar, "%c")
_pctl_define_num_assert_eq(unsigned char, uchar, "%c")
#endif

_pctl_define_num_assert_eq(short, short, "%hi")
_pctl_define_num_assert_eq(unsigned short, ushort, "%hu")
_pctl_define_num_assert_eq(int, int, "%i")
_pctl_define_num_assert_eq(unsigned int, uint, "%u")
_pctl_define_num_assert_eq(long, long, "%li")
_pctl_define_num_assert_eq(unsigned long, ulong, "%lu")

#if PCTL_USE_LONGLONG
_pctl_define_num_assert_eq(long long, llong, "%lli")
_pctl_define_num_assert_eq(unsigned long long, ullong, "%llu")
#endif

/* Floating-point equality assertions */
_pctl_define_float_assert_eq(float, float, PCTL_FLOATING_POINT_FORMAT, \
		PCTL_FLOATING_POINT_PRECISION)
_pctl_define_float_assert_eq(double, double, PCTL_FLOATING_POINT_FORMAT, \
		PCTL_FLOATING_POINT_PRECISION)
_pctl_define_float_assert_eq(long double, ldouble, "L" PCTL_FLOATING_POINT_FORMAT, \
		PCTL_FLOATING_POINT_PRECISION)

/* Complex number equality assertions */
#if PCTL_USE_COMPLEX
#include <complex.h>
/* Note: each type of complex number has its own 'creal' and 'cimag' functions */
#define _pctl_define_complex_assert_eq_(under_type, type_name, format_spec, real, imag) \
	void _pctl_assert_##type_name##_equal(under_type _Complex actual, \
		under_type _Complex expected, const char *src, const char *file, \
		const char *function, unsigned int line) \
	{ \
		if (actual == expected) { \
			register_assert_pass(); \
		} else { \
			register_assert_fail(file, function, line); \
			PCTL_PRINTF("Expected " format_spec ", but given " \
					format_spec " in\n    %s\n\n", \
					real(expected), imag(expected), \
					real(actual), imag(actual), src); \
		} \
	}
#define _pctl_define_complex_assert_eq(under_type, type_name, format_char, \
			precision, real, imag) \
	_pctl_define_complex_assert_eq_(under_type, type_name, \
		"%" _PCTL_STRINGIFY(precision) format_char " + %" \
		_PCTL_STRINGIFY(precision) format_char "i", real, imag)

_pctl_define_complex_assert_eq(float, floatc, PCTL_FLOATING_POINT_FORMAT, \
		PCTL_FLOATING_POINT_PRECISION, crealf, cimagf)
_pctl_define_complex_assert_eq(double, doublec, PCTL_FLOATING_POINT_FORMAT, \
		PCTL_FLOATING_POINT_PRECISION, creal, cimag)
_pctl_define_complex_assert_eq(long double, ldoublec, "L" PCTL_FLOATING_POINT_FORMAT, \
		PCTL_FLOATING_POINT_PRECISION, creall, cimagl)
#endif

/*
 * Bitmask assertions
 */
void _pctl_assert_bitmask_set(int result, const char *bf_src, const char *bm_src,
		const char *file, const char *function, unsigned int line)
{
	if (result) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected %s to have %s set, but it was unset\n\n",
				bf_src, bm_src);
	}
}

void _pctl_assert_bitmask_unset(int result, const char *bf_src, const char *bm_src,
		const char *file, const char *function, unsigned int line)
{
	if (result) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected %s to have %s unset, but it was set\n\n",
				bf_src, bm_src);
	}
}

/*
 * Raw memory assertions
 */
void _pctl_assert_mem_equal(const void *actual, const void *expected,
		int length, const char *a_src, const char *e_src,
		const char *file, const char *function, unsigned int line)
{
	int i;
	const char *actual_c;
	const char *expected_c;
	actual_c = (const char *)actual;
	expected_c = (const char *)expected;
	for (i = 0; i < length; i++) {
		if (actual_c[i] != expected_c[i]) {
			register_assert_fail(file, function, line);
			PCTL_PRINTF("Expected %s and %s to be equal (up to %i bytes),"
					" but they differ @ 0x%X\n"
					"    Left: 0x%X    Right: 0x%X\n\n",
					a_src, e_src, length, i,
					actual_c[i], expected_c[i]);
			return;
		} 
	}
	register_assert_pass();
}

/*
 * String assertions
 */
void _pctl_assert_string_equal(const char *actual, const char *expected,
		const char *src, const char *file, const char *function,
		unsigned int line)
{
	if (pctl_str_eq(actual, expected)) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected:\n    %s\nbut given:\n    %s\nby %s\n\n",
				expected, actual, src);
	}
}

void _pctl_assert_string_contains(const char *haystack, const char *needle,
		const char *h_src, const char *n_src, const char *file,
		const char *function, unsigned int line)
{
	if (pctl_str_contains(haystack, needle)) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected %s:\n    %s\nto contain %s:\n    %s\nbut it does not\n\n",
				h_src, haystack, n_src, needle);
	}
}

void _pctl_assert_string_doesnt_contain(const char *haystack, const char *needle,
		const char *h_src, const char *n_src, const char *file,
		const char *function, unsigned int line)
{
	if (!pctl_str_contains(haystack, needle)) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected %s:\n    %s\nto not contain %s:\n    %s\nbut it does\n\n",
				h_src, haystack, n_src, needle);
	}
}

void _pctl_assert_string_starts_with(const char *whole, const char *start,
		const char *w_src, const char *s_src, const char *file,
		const char *function, unsigned int line)
{
	if (pctl_str_starts(whole, start)) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected %s:\n    %s\nto start with %s:\n    %s\nbut it does not\n\n",
				w_src, whole, s_src, start);
	}
}

void _pctl_assert_string_ends_with(const char *whole, const char *end,
		const char *w_src, const char *e_src, const char *file,
		const char *function, unsigned int line)
{
	if (pctl_str_ends(whole, end)) {
		register_assert_pass();
	} else {
		register_assert_fail(file, function, line);
		PCTL_PRINTF("Expected %s:\n    %s\nto end with %s:\n    %s\nbut it does not\n\n",
				w_src, whole, e_src, end);
	}
}

/******************************************************************************
 * Suite and test handling                                                    *
 ******************************************************************************/
void pctl_run_suite(pctl_suite_fn suite, const char *name)
{
	executing_suite.fn = suite;
	executing_suite.name = name;
	executing_suite_failed_asserts = 0;
	suite();
	if (executing_suite_failed_asserts > 0)
		suites_failed++;
	else
		suites_passed++;
	executing_suite.fn = NULL;
}

void pctl_set_setup(pctl_setup_fn setup)
{
	executing_suite_setup = setup;
}

void pctl_set_teardown(pctl_teardown_fn teardown)
{
	executing_suite_teardown = teardown;
}

void pctl_run_test(pctl_test_fn test, const char *name)
{
	executing_test.fn = test;
	executing_test.name = name;
	executing_test_failed_asserts = 0;
	if (executing_suite_setup != NULL)
		executing_suite_setup();
	test();
	if (executing_suite_teardown != NULL)
		executing_suite_teardown();
	if (executing_test_failed_asserts > 0)
		tests_failed++;
	else
		tests_passed++;
	executing_test.fn = NULL;
}

/*
              passed  failed
     suites:       8       2
      tests:      74      12
    asserts:    2636      64
*/
void pctl_print_summary(void)
{
	PCTL_PRINTF("              passed  failed\n");
	PCTL_PRINTF("     suites:  %6i  %6i\n", suites_passed, suites_failed);
	PCTL_PRINTF("      tests:  %6i  %6i\n", tests_passed, tests_failed);
	PCTL_PRINTF("    asserts:  %6i  %6i\n", asserts_passed, asserts_failed);
}

int pctl_suites_passed(void) { return suites_passed; }
int pctl_suites_failed(void) { return suites_failed; }
int pctl_tests_passed(void) { return tests_passed; }
int pctl_tests_failed(void) { return tests_failed; }
int pctl_asserts_passed(void) { return asserts_passed; }
int pctl_asserts_failed(void) { return asserts_failed; }
